[![forthebadge](http://forthebadge.com/images/badges/built-with-love.svg)](http://forthebadge.com)
[![forthebadge](http://forthebadge.com/images/badges/cc-0.svg)](http://forthebadge.com)

## About
A list of women leaders in openness! This website contains a searchable, sortable list of women who do work in the field of openness: open access, open science, open scholarship, open source code, open data, open education resources -- anything open. There is also a map available for folks who would like to look for women leaders nearest them -- the hope is that this map makes planning conferences, workshops, and events more convenient. The data comes from [April Hathcock](https://aprilhathcock.wordpress.com/about-me/)'s [Google Doc](https://docs.google.com/document/d/1zkuWXiGKZ_pYtOgwUy0LpyqcuURVxQkwAHtHEXTigdk/edit) and merge requests to this repository.

## Building Locally
You can view this website locally once you've cloned this repository by just double clicking the `index.html` file and navigating around. However, you won't be able to see the map this way. I used Python to start up a local server to see the map along with the rest of the website. You can download Python for free [here](https://www.python.org/downloads/release/python-2710/).

Here's how to get started via the command line:
<pre><code># clone this repository (you will need git installed for this; you can also download the repo as a .zip)
$ git clone git@gitlab.com:VickyRampin/Women-Leaders-Openness.git

# start up a local server
$ python -m SimpleHTTPServer 8000
</pre></code>

Open up an Internet browser, and go to `localhost:8000`. You should now be able to see and interact with the website, including the map, locally!

## Tech I'm Using
### Get Data
To start, I grab data from [April Hathcock](https://aprilhathcock.wordpress.com/about-me/)'s [Google Doc](https://docs.google.com/document/d/1zkuWXiGKZ_pYtOgwUy0LpyqcuURVxQkwAHtHEXTigdk/edit) which contains a crowd-sourced list of women who work in openness, and throw it into a separate Google sheet. Google Sheets provides some basic functionality I use to get the right data in the right columns, such as splitting columns on different values (Data > Split text to columns), such as commas. There is some manual clean up required after this to make sure everything fits into the 3 desired columns (at this point), 'Name', 'Twitter Handles', and 'Affiliation'.

### Clean Data
When that's finished, I download the data as a `.csv` file. I then start up [OpenRefine](http://openrefine.org/) to geocode people based on their institutional affiliation. I used the steps from OpenRefine's [Geocoding wiki](https://github.com/OpenRefine/OpenRefine/wiki/Geocoding) to do so, specifically the "Fetching" and "Parsing" functions. 

Basically, in OpenRefine, I click the dropdown menu from the "Affiliation" column, and select Edit Column > Add Column by Fetching URLs. In the text box, I input the formula: `http://maps.google.com/maps/api/geocode/json?sensor=false&address=" + escape(value, "url")`. I name the new column to be created "geo" or something of that ilk, and hit ok. Depending on how much data is there (I don't do the whole spreadsheet everytime, just incremementally as new data comes in), it could take a while. It's also subject to Google Maps rate limiting, so if you have a *ton* of data, it will take a long while.

After it's finished, there's a new column called "geo" with a ton of JSON. Select Edit Column > Add Column based on This Column and input this formula in the text box: `with(value.parseJson().results[0].geometry.location, pair, pair.lat +", " + pair.lng)`. I name the new column "latlog" since it'll contain both values, separated by a comma. This will generate a new column with just the lat/long values.

Once that's finished, you can Edit Column > Split Into Several Columns, and then accept the defaults (it splits on a comma). I then have two columns, latlong and latlong(1). I rename those to "lat" and "long" and delete the "geo" and "latlong" columns that still exist. The only remaining columns I have at that point are 'Name', 'Twitter Handles', 'Affiliation', 'Lat', and 'Long'. I export that as a `.csv` and put it into `data/women-leaders-openness.csv`.

### Making the Website
#### General Tools for Website
I built the website using [Bootstrap](https://getbootstrap.com), the open source toolkit for developing with HTML, CSS, and JS. I modified the [Flat](https://bootswatch.com/flatly/) theme from [Bootswatch](https://bootswatch.com/) for the website's look. All customizations are available in `css/custom.css` however the main change is the color -- from the default dark blue to the current light purple. I didn't change any of the original Bootstrap javascript files (all in the `js` folder). 

The code to render the searchable, sortable table on the [home page](https://fyoaw.vickysteeves.com) was adapted from 'CSV to HTML table' by Derek Eder, available here: https://github.com/derekeder/csv-to-html-table. I also modified the CSS , found in `css/dataTables.bootstrap.css`. I did not modify the JavaScript files from this repository, which are `js/csv_to_html_table.js` and `js/leaflet/dataTables.bootstrap.js`.

I am also using the JQuery, which leaflet and other libraries depend on. I am just using the default, found in the `js` folder.

#### Map
The [map](https://fyoaw.vickysteeves.com/map.html) was built using [leaflet](http://leafletjs.com/) open source JavaScript library used to build interactive maps on the web. You can find the leaflet javascript files in the `js/leaflet/` file folder, which was downloaded as is from the website. The actual script to build the map is at the bottom `map.html` and also here:

<pre><code><script type="text/javascript">
var csv_path = "data/women-leaders-openness.csv";
var map = L.map("map");
$.when($.get(csv_path)).then(function(data) {
    var csv_data = $.csv.toArrays(data, {separator: ",", delimiter: '"'});

    // default view, center of the world, zoom set to 3
    map.setView([51.505, -0.09], 3);
	
    // make a title for the map, add a license, taken directly from leaflet default
    L.tileLayer("http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
        maxZoom:15, attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/publicdomain/mark/1.0/">CC0</a>'}).addTo(map);;

    // loops through the entire CSV
    for(var i = 1; i < csv_data.length; i++) {
        var row = csv_data[i];
	
	// if you click on a point on the map, the name (row[0]) will show on the popup
        var popup = "<b>" + row[0] + "</b>";
	
	// if the person has a twitter handle, it will also show up on the popup        
	if(row[1]) {
            popup += ", " + row[1];
        }

	// affiliation will also show up on popup if it's there
        popup += "<br>" + row[2];

	// this places the marker were rows 3 and 4 (lat and long on our csv) say
        L.marker([row[3], row[4]], {title: row[0]}).addTo(map).bindPopup(popup);
    }
});
    </script>
</pre></code>

In this case, I am using the same `.csv` file to build the searchable table on the home page and the map by including all the data there. This script loops through the CSV, looks for any information that would go in the popup (row[0] = Name, row[1] = Twitter Handle, row[2]=Affiliation) and places the marker on the map based on the lat (row[3]) and long (row[4]) on the sheet.

#### Building & Deploying Website
I am also using [GitLab](https://about.gitlab.com/) for version control, building the site (using the native [Continuous Integration](https://about.gitlab.com/features/gitlab-ci-cd/)), and serving the website. To do this via GitLab, I need to add a `.gitlab-ci.yml` file to my repository ([here](https://gitlab.com/VickyRampin/Women-Leaders-Openness/blob/master/.gitlab-ci.yml)), which looks like this:
<pre><code># This file is a template, and might need editing before it works on your project.
# Full project: https://gitlab.com/pages/plain-html

image: "python:2"

stages:
  - test
  - deploy

check:
  stage: test
  script:
  - python checkcsv.py data/women-leaders-openness.csv

pages:
  stage: deploy
  script:
  - mkdir .public
  - cp -r * .public/
  - mv .public public
  - "sed 's/\\( csv_path *[:=] *[\"'\\'']data\\/women-leaders-openness\\.csv\\)\\([\"'\\'']\\)/\\1?h='$(git rev-parse --short HEAD)'\\2/' index.html >public/index.html"
  - "sed 's/\\( csv_path *[:=] *[\"'\\'']data\\/women-leaders-openness\\.csv\\)\\([\"'\\'']\\)/\\1?h='$(git rev-parse --short HEAD)'\\2/' map.html >public/map.html"
  artifacts:
    paths:
    - public
  only:
  - master
</pre></code>

This builds the website in two stages: test and deploy. During the "test" phase, GitLab deploys a script `checkcsv.py` (found [here](https://gitlab.com/VickyRampin/Women-Leaders-Openness/blob/master/checkcsv.py) in the repo) to basically check I didn't mess up the CSV when I updated it. If that passes, then it'll go onto the "deploy" phase. This actually builds and serves the website to the public. 

## Contribute!
If you'd like to contribute, that would be amazing! I will try my best to be on top of merge requests and issues. If you aren't comfortable with coding or git, you can email me ([vicky.rampin@nyuedu](mailto:vicky.rampin@nyu.edu)) changes you'd like to see and I will open an issue for you or, if it's quick enough, make the change and give you attribution on this README.


Here are some general instructions to help -- the following was adapted from [ProjectPorcupine's](https://github.com/TeamPorcupine/ProjectPorcupine)'s [CONTRIBUTING.md](https://github.com/TeamPorcupine/ProjectPorcupine/blob/master/CONTRIBUTING.md).

Pease follow the [Contributor Covenant](CODE_OF_CONDUCT.md) in all your interactions with the project. If you would like to contribute to this project by modifying/adding to the code or data, please feel free to follow the standard GitLab workflow:

1. Fork the project (the second button to the left under the title of the repo)
2. Clone your fork to your computer.
 * From the command line: `git clone https://gitlab.com/<USERNAME>/Women-Leaders-Openness.git`
3. Change into your new project folder.
 * From the command line: `cd Women-Leaders-Openness`
4. [optional]  Add the upstream repository to your list of remotes.
 * From the command line: `git remote add upstream git@gitlab.com:VickyRampin/Women-Leaders-Openness.git`
5. Create a branch for your new feature.
 * From the command line: `git checkout -b my-feature-branch-name`
6. Make your changes.
 * Avoid making changes to more files than necessary for your feature (i.e. refrain from combining your "real" pull request with incidental bug fixes). This will simplify the merging process and make your changes clearer.
7. Commit your changes. From the command line:
 * `git add <FILE-NAMES>`
 * `git commit -m "A descriptive commit message"`
8. While you were working some other changes might have gone in and break your stuff or vice versa. This can be a *merge conflict* but also conflicting behavior or code. Before you test, merge with master.
 * `git fetch upstream`
 * `git merge upstream/master`
9. Test. Run the program and do something related to your feature/fix.
10. Push the branch, uploading it to GitLab.
  * `git push origin my-feature-branch-name`
11. Make a "Merge Request" from your branch here on GitLab.

### Best Practices for Contributing

* Before you start coding, open an issue so that the community can discuss your change to ensure it is in line with the goals of the project and not being worked on by someone else. This allows for discussion and fine tuning of your feature and results in a more succent and focused additions.
    * If you are fixing a small glitch or bug, you may make a MR without opening an issue.
    * If you are adding a large feature, create an issue so that we may give you feedback and agree on what makes the most sense for the project before making your change and submitting a MR (this will make sure you don't have to do major changes down the line).

* Merge Requests are eventually merged into the codebase. Please ensure they are:
    * Well tested by the author. It is the author's job to ensure their code works as expected.

* If your code is untested, log heavy, or incomplete, you can use GitLab's [Work In Progress (WIP) feature](https://docs.gitlab.com/ce/user/project/merge_requests/work_in_progress_merge_requests.html) on your merge request so others know it is still being tested and shouldn't be considered for merging yet. This way we can still give you feedback or help finalize the feature even if it's not ready for prime time.

That's it! Thanks for your contribution!

## Contact info

You are welcome to email me at [vicky.rampin@nyu.edu](mailto:vicky.rampin@nyu.edu) if you have questions or concerns, or raise an issue on this repository and I will do my best to respond quickly!

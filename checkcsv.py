#!/usr/bin/env python

import csv
import re
import sys


def error(msg, line):
    sys.stderr.write(msg)
    sys.stderr.write(" line %d\n" % (line + 1))
    sys.exit(1)


_twitter = re.compile(r'^(@[a-zA-Z0-9_]{1,15})?$')
_coord = re.compile(r'^-?[0-9]{1,3}\.[0-9]{1,10}$')


if __name__ == '__main__':
    with open(sys.argv[1]) as fp:
        cols = None
        for line, row in enumerate(csv.reader(fp)):
            if line == 0:
                cols = len(row)
                continue
            if len(row) != cols:
                error("Invalid number of columns", line)
            if not _twitter.match(row[1]):
                error("Invalid Twitter handle %r" % row[1], line)
            if not _coord.match(row[3]):
                error("Invalid latitude %r" % row[3], line)
            if not _coord.match(row[4]):
                error("Invalid longitude %r" % row[4], line)
        print("Validated %d entries" % line)
